import { user } from "../models/User"

type Action = { type: string, payload?: any }

export const Actions = {
    logIn: 'LOG_IN',
    registr: 'REGISTRATION'
};

interface State {
    user: user | null;
}

const initialState: State = {
    user: null
}

const userReducer = (state = initialState, action: Action) => {
    console.log(action.type);
    switch (action.type) {
      case Actions.logIn:
      case Actions.registr:
        return { ...state, user: action.payload };
      default:
        return state;
    }
};

export default userReducer;