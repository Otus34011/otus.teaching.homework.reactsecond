import { configureStore } from '@reduxjs/toolkit';
import userReducer from './state/Reducer';

const Store = configureStore({
    reducer: {
        userStorage: userReducer,
    }
});

export default Store;