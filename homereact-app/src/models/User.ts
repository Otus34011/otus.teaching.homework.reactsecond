export interface user {
    login: string;
    password: string;
    email?: string;
}