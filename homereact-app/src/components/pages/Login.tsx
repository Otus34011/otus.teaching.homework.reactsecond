import '../../App.css';
import { useState } from 'react';
import { Button, Link, TextField } from "@mui/material";
import { useDispatch } from "react-redux";
import { useNavigate } from 'react-router-dom';
import { 
  inputStyle,
  buttonStyle,
  linkStyle 
} from '../../styles/Styles';

const Login = () => {

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [login, setLogin] = useState('');
  const [password, setPassword] = useState(''); 

  const handleClick = (state: string) => {
    
    if (login === '' || password === '')
    {
      navigate('/notfound');
      return;
    }

    dispatch({
      type: 'LOG_IN',
      payload: {
        login: login,
        password: password
      }
    });

    navigate('/', {state});
  }; 

  return (     
    <form>
      <div className='App'>
        <h1>Login</h1>        
        <TextField 
          style={inputStyle} 
          onChange={e => setLogin(e.target.value)}
          label="Login" 
          variant="outlined" />
        <br />
        <TextField
          style={inputStyle}
          onChange={e => setPassword(e.target.value)}
          label="Password"
          type="password"
          autoComplete="current-password" 
          variant="outlined" />
        <br />
        <Button
          style={buttonStyle}
          variant={'contained'} 
          onClick={() => handleClick(login)}>Login</Button>  
        <br />
        <Link
          style={linkStyle}
          variant="body2"
          href="/register">
          Registration
        </Link>     
        <br/>   
      </div>           
    </form>  
  );
}

export default Login;
