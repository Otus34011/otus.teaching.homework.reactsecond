import '../../App.css';
import { Link } from "@mui/material";
import { linkStyle } from '../../styles/Styles';

const NotFound = () => {
    return (     
      <form>
        <div className='App'>
          <h1>Not found</h1>
          <Link
            style={linkStyle}
            variant="body2"
            href="/register">
            Registration
          </Link>     
          <br/>   
        </div>           
  </form>  
    );
}

export default NotFound;