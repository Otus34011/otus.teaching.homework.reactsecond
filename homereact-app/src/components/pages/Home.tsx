import '../../App.css';
import {useLocation} from 'react-router-dom';

interface Props {
  name: string;
}

const Home = (props: Props) => {

  const location = useLocation();

  return (
    <div className='App'>
      <h1>Welcome</h1>
      <span>{location.state}</span>
    </div>      
  );
};

export default Home;
