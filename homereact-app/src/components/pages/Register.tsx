import '../../App.css';
import { useState } from 'react';
import {Button, Link, TextField} from "@mui/material";
import { useDispatch } from "react-redux";
import { useNavigate } from 'react-router-dom';
import { 
  inputStyle,
  buttonStyle,
  linkStyle 
} from '../../styles/Styles';

const Register = () => {

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [login, setLogin] = useState('');
  const [password, setPassword] = useState(''); 
  const [email, setEmail] = useState('');   

  const handleClick = (state: string) => {

    if (login === '' || password === '' || email === '')
    {
      alert('Data is empty. Please fill the data.'); 
      return;
    }

    dispatch({
      type: 'REGISTRATION',
      payload: {
        login: login,
        password: password,
        email: email
      }
    });

    navigate('/', {state});
  };   
           
  return (  
    <form >
      <div className='App'>
        <h1>Register</h1>
        <TextField  
          style={inputStyle} 
          onChange={e => setLogin(e.target.value)}
          label="Login"
          variant="outlined" 
          required/>
        <br />         
        <TextField 
          style={inputStyle} 
          onChange={e => setPassword(e.target.value)}
          autoComplete="current-password"
          label="Password"
          type="password"
          variant="outlined" 
          required/>
        <br />  
        <TextField 
          style={inputStyle} 
          onChange={e => setEmail(e.target.value)}
          label="Email"
          variant="outlined" 
          required/>
        <br />            
        <Button 
          style={buttonStyle}
          onClick={() => handleClick(login)}
          variant={'contained'}>
          Save
        </Button>           
        <br/>                  
        <Link 
          style={linkStyle}
          className="link_field"
          variant="body2"
          href="/login">
          Log In
        </Link>     
        <br/>
      </div>    
    </form>    
  );
}

export default Register;
